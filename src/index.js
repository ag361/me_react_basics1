import './index.css';
import React from "react";
import ReactDOM from "react-dom";

const element = React.createElement("h1", null, "React element");

console.log(element);

ReactDOM.render(element, document.querySelector("#root"));

